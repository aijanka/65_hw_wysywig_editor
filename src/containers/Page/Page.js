import React, {Component, Fragment} from 'react';
import axios from 'axios';
import {Jumbotron} from "react-bootstrap";

class Page extends Component {
    state = {
        page: [],
        serverKey: ''
    };

    componentDidMount() {
        this.getPage();

    }

    getPage = () => {
        axios.get('/pages.json').then(response => {
            const serverKey = this.props.match.params.page;
            const page = response.data[serverKey];
            this.setState({page, serverKey});

        })
    }

    componentDidUpdate() {
        if(this.state.serverKey !== this.props.match.params.page){
            this.getPage();
        }
    }

    render() {
        return (
            <Fragment>
                <Jumbotron>
                    <h1>{this.state.page.title}</h1>
                    <p>{this.state.page.content}</p>
                    <p>
                        {/*<Button bsStyle="primary">Learn more</Button>*/}
                    </p>
                </Jumbotron>
                {/*<NavLink to='/aboutUs/edit'>Edit</NavLink>*/}
            </Fragment>

        )
    }
}

export default Page;