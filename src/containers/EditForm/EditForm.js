import React, {Component} from 'react';
import axios from 'axios';
import {Button, ControlLabel, FormControl, FormGroup} from "react-bootstrap";

class EditForm extends Component {
    state = {
        currentInfo: {
            title: '',
            content: ''
        },
        pageName: ''
    };

    componentDidMount() {
        const pageName = this.props.match.params.page;
        this.setState({pageName});

        axios.get('/pages.json').then(response => {
            this.setState({currentInfo: response.data[pageName]}, () => {
                console.log(this.state)
            });
        })
    }

    saveInfoToServer = () => {
        axios.patch(`/pages/${this.state.pageName}.json`, this.state.currentInfo).then(() => {
            this.props.history.push('/pages/' + this.state.pageName);
        })
    }

    saveInputChange = event => {
        const currentInfo = {...this.state.currentInfo};
        currentInfo[event.target.name] = event.target.value;

        this.setState({currentInfo});
    }

    render() {
        return (
            <form>
                <FormGroup controlId="formControlsTextarea">
                    <ControlLabel>Title</ControlLabel>
                    <FormControl
                        componentClass="textarea"
                        placeholder="Title"
                        value={this.state.currentInfo.title}
                        name='title'
                        onChange={this.saveInputChange}
                    />
                </FormGroup>

                <FormGroup controlId="formControlsTextarea">
                    <ControlLabel>Content</ControlLabel>
                    <FormControl
                        componentClass="textarea"
                        placeholder="Content"
                        value={this.state.currentInfo.content}
                        name='content'
                        onChange={this.saveInputChange}
                    />
                </FormGroup>
                <Button onClick={this.saveInfoToServer}>Save</Button>
            </form>
        )
    }
} export default EditForm;