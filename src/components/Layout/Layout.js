import React from 'react';
import Header from './Header/Header';
import './Layout.css';

const Layout = props => (
    <div className="container">
        <Header/>
        <main>{props.children}</main>
    </div>
);

export default Layout;