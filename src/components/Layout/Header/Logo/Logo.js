import React from 'react';
import './Logo.css';

import neobisLogo from '../../../../assets/images/whiteIcon.jpg';
import {NavLink} from "react-router-dom";

const Logo = () => (
    <div className="Logo">
        <NavLink to='/pages/aboutUs' exact>
            <img src={neobisLogo} alt="Neobis Logo"/>
        </NavLink>
    </div>
);

export default Logo;