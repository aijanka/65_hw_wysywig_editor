import React from 'react';
import Logo from "./Logo/Logo";
import NavigationItems from "./NavigationItems/NavigationItems";

const Header = props => {

    return (
        <header className='Header'>
            <Logo/>
            <NavigationItems/>
        </header>
    );
}

export default Header;