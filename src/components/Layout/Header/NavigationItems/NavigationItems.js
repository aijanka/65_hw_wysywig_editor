import React, {Component} from 'react';
import axios from 'axios';
import NavigationItem from "./NavigationItem/NavigationItem";

class NavigationItems extends Component {

    state = {
        pages: []
    };

    componentDidMount() {
        this.getPages();

    }

    getPages() {
        axios.get('/pages.json').then(response => {
            const pages = [];
            const data = response.data;
            for(let page in data) {
                pages.push({
                    id: page,
                    title: data[page].title,
                    serverKey: page
                })
            }
            this.setState({pages});
        })
    }


    render() {
        return (
            <ul>
                {this.state.pages.map(page => (<NavigationItem
                                                    to={`/pages/${page.serverKey}`}
                                                    key={page.id}
                                                            >{page.title}</NavigationItem>))}
                <NavigationItem to='/pages/admin' exact>Edit some page</NavigationItem>
            </ul>
        );
    }
};


export default NavigationItems;