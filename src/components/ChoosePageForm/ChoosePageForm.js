import React, {Component} from 'react';
import {ControlLabel, FormControl, FormGroup} from "react-bootstrap";

class ChoosePageForm extends Component{



    saveCurrentChoice = event => {
        event.preventDefault();
        console.log(event.target.value);
        this.props.history.push(`/pages/admin/${event.target.value}`);

    }

    render(){
        return (
            <form>
                <FormGroup controlId="formControlsSelect"  onChange={this.saveCurrentChoice}>
                    <ControlLabel>Choose page to edit</ControlLabel>
                    <FormControl componentClass="select" placeholder="choose page">
                        <option>Choose page to edit</option>});
                        <option value='aboutUs' >About Us</option>});
                        <option value='projects' >Our projects</option>});
                        <option value='contacts' >Our contacts</option>});
                        <option value='awards' >Our awards</option>});
                        <option value='meetups' >Our meetups</option>});
                    </FormControl>
                </FormGroup>
            </form>
        )
    }
}

export default ChoosePageForm;
