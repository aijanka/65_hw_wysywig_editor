import React, {Component} from 'react';
import Layout from "./components/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Page from "./containers/Page/Page";
import ChoosePageForm from "./components/ChoosePageForm/ChoosePageForm";
import EditForm from "./containers/EditForm/EditForm";

class App extends Component {
    render() {
        return (
                <Layout>
                    <Switch>
                        <Route path='/pages/admin/:page' exact component={EditForm}/>
                        <Route path='/pages/admin' exact component={ChoosePageForm}/>
                        <Route path='/pages/:page' exact component={Page}/>
                        <Route path='/' render={() => <h1>not found</h1>}/>
                    </Switch>
                </Layout>

        );
    }
}

export default App;
